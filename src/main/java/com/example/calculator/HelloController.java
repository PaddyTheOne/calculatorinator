package com.example.calculator;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class HelloController {

    private int result = 0;
    private String calText = "";
    private String resText = "";
    private String symbolText = "";

    @FXML
    public Text TextCal;
    @FXML
    public Text TextResult;

    @FXML
    public VBox VBoxHistory;
    @FXML
    public ScrollPane ScrollPaneHistory;


    @FXML
    protected void onButtonDigitClick(Event e){

        calText = ((Button) e.getSource()).getText();
        if (TextCal.getText().equals("0")){
            TextCal.setText(calText);
        } else {
            if (TextCal.getText().equals("(")||TextCal.getText().equals(")")){
                TextCal.setText(TextCal.getText()+calText);
            } else {
                TextCal.setText(TextCal.getText()+calText);
            }
        }
    }


    @FXML
    protected void onButtonSymbolClick(Event e){

        symbolText = ((Button) e.getSource()).getText();
        if (TextCal.getText().equals("0")){
            return;
        } else {
            try {
                if (Double.parseDouble(TextCal.getText().substring(TextCal.getText().length() - 1)) >= 0) {
                    TextCal.setText(TextCal.getText() + symbolText);
                }
            } catch (Exception ex) {
                if (TextCal.getText().substring(TextCal.getText().length() - 1).equals("(")||TextCal.getText().substring(TextCal.getText().length() - 1).equals(")")) {
                    TextCal.setText(TextCal.getText() + symbolText);
                }
            }
        }
    }

    @FXML
    protected void onButtonClearClick(){
        TextResult.setText("0");
        TextCal.setText("0");
    }

    @FXML
    protected void onButtonClearEntryClick(){
        TextCal.setText("0");
    }

    @FXML
    protected void onButtonRemoveClick(){
        if (TextCal.getText().equals("0")){
            return;
        } else {
            if (TextCal.getText().length()==1){
                TextCal.setText("0");
            } else {
                TextCal.setText(TextCal.getText().substring(0,TextCal.getText().length()-1));
            }
        }
    }

    @FXML
    protected void onButtonCommaClick(){
        if (TextCal.getText().endsWith("+")||TextCal.getText().endsWith("-")||TextCal.getText().endsWith("/")||TextCal.getText().endsWith("*")||TextCal.getText().endsWith(".")||TextCal.getText().endsWith("(")||TextCal.getText().endsWith(")")){
            return;
        } else {
            if (isCommaAllowed(TextCal.getText())){
                TextCal.setText(TextCal.getText()+".");
            }
        }
    }

    private boolean isCommaAllowed(String calText){

        String c;

        if (calText.length()>=0 && calText.length()<3) {
            c = calText;
        } else {
            c = calText.substring(calText.length()-2);
        }

        if (c.contains(".")){
            return false;
        } else {
            return true;
        }

    }

    @FXML
    protected void onButtonCloseClick(){
        System.exit(0);
    }

    @FXML
    protected void onButtonClearSavesClick(){
        VBoxHistory = new VBox();
        Separator SeparatorForHistory = new Separator();

        SeparatorForHistory.setPrefHeight(10);

        Text TextCalHistory = new Text("Calculation History");

        TextCalHistory.setStyle("-fx-font: 16 System;");

        TextCalHistory.setWrappingWidth(195);

        TextCalHistory.setTextAlignment(TextAlignment.CENTER);

        VBoxHistory.getChildren().add(TextCalHistory);
        VBoxHistory.getChildren().add(SeparatorForHistory);

        ScrollPaneHistory.setContent(VBoxHistory);
    }


    @FXML
    protected void onButtonEqualsClick(){

        TextResult.setText(""+(Double.parseDouble(TextResult.getText())+calculateValue(TextCal.getText())));

        AddToHistory(TextCal.getText(),Double.parseDouble(TextResult.getText()));
        amountOfHistory++;
    }

    private void AddToHistory(String Cal,Double Result){

        Text TextTypeCal = new Text(""+Cal);
        Text TextTypeResult = new Text(""+Result);

        TextTypeCal.setStyle("-fx-font: 12 arial;");
        TextTypeResult.setStyle("-fx-font: 16 arial;");

        TextTypeCal.setWrappingWidth(170);
        TextTypeResult.setWrappingWidth(170);

        TextTypeCal.setTextAlignment(TextAlignment.RIGHT);
        TextTypeResult.setTextAlignment(TextAlignment.RIGHT);

        VBoxHistory.getChildren().add(TextTypeCal);
        VBoxHistory.getChildren().add(TextTypeResult);
        VBoxHistory.getChildren().add(new Separator());

        ScrollPaneHistory.setContent(VBoxHistory);
    }

    @FXML
    public void ScrollAuto(){
        ScrollPaneHistory.setVvalue(ScrollPaneHistory.getVmax());
    }

    @FXML
    public void OnButtonSaveValueClick() throws FileNotFoundException{
        writeFileSingleLine();
    }

    @FXML
    public void OnButtonLoadValueClick() throws FileNotFoundException{
        readFileSingleLine();
    }

    @FXML
    public void OnButtonSaveHistoryValueClick() throws FileNotFoundException{
        writeFileMultiLine();
    }

    @FXML
    public void OnButtonLoadHistoryValueClick() throws FileNotFoundException{
        readFileMultiLine();
    }








    private double calculateValue(String calText){

        positionsOfSymbols.clear();
        positionsOfSymbols.add(0);

        amountOfStartBrackets=0;
        amountOfEndBrackets=0;

        amountOfTimes=0;
        amountOfDivided=0;
        amountOfPlus=0;
        amountOfMinus=0;

        System.out.println(calText);
        String stringCal=calText;

        runThroughCalText(stringCal);

        if (amountOfStartBrackets==amountOfEndBrackets){
            //calBrackets(calText);
        } else {
            System.out.println("ERROR: Amount of brackets dont match");
            return 0;
        }


        System.out.println("Done");
        calPlus(calText);


        return result;
    }

    ArrayList<Integer> positionsOfSymbols = new ArrayList<>();

    int amountOfStartBrackets;
    int amountOfEndBrackets;

    int amountOfTimes;
    int amountOfDivided;
    int amountOfPlus;
    int amountOfMinus;

    private void runThroughCalText(String stringCal){
        for (int i = 0; i<stringCal.length();i++){
            String s = stringCal.substring(i,i+1);
            System.out.println(stringCal.substring(i,i+1));
            switch (s){
                case(")"):
                    amountOfEndBrackets++;
                    positionsOfSymbols.add(i);
                    break;
                case("("):
                    amountOfStartBrackets++;
                    positionsOfSymbols.add(i);
                    break;
                case("*"):
                    amountOfTimes++;
                    positionsOfSymbols.add(i);
                    break;
                case("/"):
                    amountOfDivided++;
                    positionsOfSymbols.add(i);
                    break;
                case("+"):
                    amountOfPlus++;
                    positionsOfSymbols.add(i);
                    break;
                case("-"):
                    amountOfMinus++;
                    positionsOfSymbols.add(i);
                    break;

            }
        }

        positionsOfSymbols.add(stringCal.length()-1);

        System.out.println(amountOfStartBrackets);
        System.out.println(amountOfEndBrackets);

        System.out.println(amountOfTimes);
        System.out.println(amountOfDivided);
        System.out.println(amountOfPlus);
        System.out.println(amountOfMinus);
    }

    private String calBrackets(String calText){

        return calText;
    }

    private String calTimes(String calText){

        return calText;
    }

    private String calDivided(String calText){

        return calText;
    }

    private Double calPlus(String calText){

        ArrayList<Integer> positionsOfPlus = new ArrayList<>();
        double value=0;

        for (int i = 0; i<calText.length(); i++){
            if (calText.substring(i,i+1).equals("+")){
                positionsOfPlus.add(i);
            }
        }

        System.out.println(positionsOfSymbols);
        System.out.println(positionsOfPlus);

        for (int i = 0; i<amountOfPlus; i++){
            for (int j = 0; j<positionsOfSymbols.size();j++){
                if (positionsOfSymbols.get(j).equals(positionsOfPlus.get(i))){

                    System.out.println(Double.parseDouble(calText.substring(positionsOfSymbols.get(j-1))));
                    System.out.println(Double.parseDouble(calText.substring(positionsOfSymbols.get(j))));

                    //value = Double.parseDouble(calText.substring(positionsOfSymbols.get(j)));
                }
            }
        }

        return value;
    }

    private String calMinus(String calText){

        return calText;
    }





    private final String delimiter = ";";

    private void readFileSingleLine() throws FileNotFoundException {
        File file = new File("C:\\Users\\Patrick\\IdeaProjects\\Calculator\\Save\\Calculator Save.txt");
        Scanner scanner = new Scanner(file);
        boolean OneTwo=false;

        scanner.useDelimiter(delimiter);

        while (scanner.hasNext())
        {
            if (!OneTwo){
                TextCal.setText(scanner.next());
                OneTwo=true;
            } else {
                TextResult.setText(scanner.next());
                OneTwo=false;
            }
        }
        scanner.close();
    }


    private void writeFileSingleLine() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter("C:\\Users\\Patrick\\IdeaProjects\\Calculator\\Save\\Calculator Save.txt");

        pw.println(TextCal.getText()+delimiter+TextResult.getText());
        pw.close();
    }

    private int amountOfHistory=0;

    private void readFileMultiLine() throws FileNotFoundException{
        File file = new File("C:\\Users\\Patrick\\IdeaProjects\\Calculator\\Save\\Calculator History Save.txt");
        Scanner scanner = new Scanner(file);
        boolean OneTwo=false;

        scanner.useDelimiter(delimiter);

        while (scanner.hasNext())
        {
            if (!OneTwo){
                TextCal.setText(scanner.next());
                OneTwo=true;
            } else {
                TextResult.setText(scanner.next());
                OneTwo=false;
            }
        }
        scanner.close();
    }

    private void writeFileMultiLine() throws FileNotFoundException{
        PrintWriter pw = new PrintWriter("C:\\Users\\Patrick\\IdeaProjects\\Calculator\\Save\\Calculator History Save.txt");

        pw.println(amountOfHistory);

        for (int i = 0; amountOfHistory>i ; i++){
            pw.println(TextCal.getText()+delimiter+TextResult.getText());

            ScrollPaneHistory.getContent();
        }

        pw.close();
    }







}